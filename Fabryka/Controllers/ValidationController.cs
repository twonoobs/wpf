﻿using System.Collections.Generic;
using System.Linq;
using Db4objects.Db4o;
using Fabryka.Models;
using Fabryka.Properties;

namespace Fabryka.Controllers
{
    public sealed class ValidationController
    {
        public bool CheckList(IEnumerable<Component> components, Component searchComponent)
        {
            return components.Where(x => x.Parent != null).Any(x => x.Parent.Name != searchComponent.Parent.Name);
        }

        public bool CheckUp(ComponentList parent, ComponentList child)
        {
            if (child?.SubComponents == null) return false;

            foreach (var item in child.SubComponents)
            {

                if (parent == item.Parent)
                {
                    return true;
                }
                else
                {
                    CheckUp(parent, item.Parent);
                }
            }

            return false;
        }

        public bool CheckDown(ComponentList parent, ComponentList child)
        {
            return child?.SubComponents != null && child.SubComponents.Any(item => CheckUp(parent, item.Parent));
        }


        public bool CheckContainComponentList(ComponentList parent)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                foreach (var item in container.Query<ComponentList>().ToList())
                {
                    if (CheckItem(item, parent))
                    {
                        return true;
                    }
                }
            }

            return false;           
        }

        private bool CheckItem(ComponentList element, ComponentList goodElement)
        {
            if (element?.SubComponents == null) return false;

            var exist = false;

            foreach (var item in element.SubComponents)
            {
                if (item.Parent.Name == goodElement.Name)
                {
                    exist = true;
                }

                if (!exist)
                {
                    exist = CheckItem(item.Parent, goodElement);
                }
            }

            return exist;
        }
    }
}
