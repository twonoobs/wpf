﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Db4objects.Db4o;
using Fabryka.Models;
using Fabryka.Properties;

namespace Fabryka.Controllers
{
    public sealed class DeleteController
    {
        private DatabaseController DatabaseController { get; }
        private ValidationController ValidationController { get; }

        public DeleteController()
        {
            DatabaseController = new DatabaseController();;
            ValidationController=new ValidationController();
        }

        public void DeleteComponent(ComponentList list)
        {
            if (ValidationController.CheckContainComponentList(list))
            {
                if (MessageBox.Show("Lista składowa jest składnikiem innych list składowych czy chcesz ją skasować na wszystkich jej pozycjach?","Usuwanie listy składowej", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    var query = new List<ComponentList>();

                    using (var container = Db4oEmbedded.OpenFile(Resources.Database))
                    {
                        query.AddRange(container.Query<ComponentList>());
                    }

                    foreach (var item in query)
                    {
                        DeleteComponentListReferences(item, list);
                    }

                    DatabaseController.RemoveComponentList(list);
                }      
            }
            else
            {
                DatabaseController.RemoveComponentList(list);
            }
        }

        private void DeleteComponentListReferences(ComponentList parent, ComponentList search)
        {
            if (parent?.SubComponents == null) return;

            foreach (var item in parent.SubComponents)
            {
                if (item.Parent.Name == search.Name)
                {
                    DatabaseController.RemoveReference(parent,search);
                }
                else
                {
                    DeleteComponentListReferences(item.Parent, search);
                }
            }
        }

        public void DeleteComponent(ComponentList parent, ComponentList children)
        {
            DatabaseController.RemoveComponent(parent,children);
        }
    }
}
