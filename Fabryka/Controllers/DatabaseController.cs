﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Db4objects.Db4o;
using Fabryka.Models;
using Fabryka.Properties;

namespace Fabryka.Controllers
{
    public sealed class DatabaseController
    {
        #region ComponentList

        public void AddComponentList(ComponentList list)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                container.Store(list);
                container.Commit();
            }
        }

        public void EditComponentList(ComponentList newValue, string previousValue)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                var temp = container.Query<ComponentList>().First(x => x.Name == previousValue);
                temp.Name = newValue.Name;
                temp.MeasureUnit = newValue.MeasureUnit;
                container.Store(temp);
                container.Commit();
            }
        }

        public void RemoveComponentList(ComponentList list)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                container.Delete(container.Query<ComponentList>().First(x => x.Name == list.Name));
                container.Commit();
            }
        }

        public IEnumerable<ComponentList> GetComponentList()
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                return container.Query<ComponentList>().OrderBy(x => x.Name).ToList();
            }
        }

        public bool CanAddComponentList(ComponentList list)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                return container.Query<ComponentList>().All(x => x.Name != list.Name);
            }
        }

        #endregion

        #region Component

        public void AddComponentWithList(ComponentList list,Component children)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                var parent = container.Query<ComponentList>().First(x => x.Name == list.Name);

                if(parent.SubComponents == null) parent.SubComponents = new List<Component>();

                children.Parent = container.Query<ComponentList>().First(x => x.Name == children.Parent.Name);
                parent.SubComponents.Add(children);

                container.Store(parent);
                container.Store(parent.SubComponents);
                container.Commit();
            }
        }

        public void EditQComponentQuantity(ComponentList parent,ComponentList children,double quantity)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                var dbParent = container.Query<ComponentList>().First(x => x.Name == parent.Name);

                foreach (var subComponent in dbParent.SubComponents)
                {
                    if (subComponent.Parent.Name == children.Name)
                    {
                        subComponent.Quantity = quantity;
                        container.Store(subComponent);
                        container.Store(subComponent.Parent);
                        container.Commit();
                        break;
                    }
                }
            }
        }

        public void RemoveReference(ComponentList parent, ComponentList children)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                var dbParent = container.Query<ComponentList>().First(x => x.Name == parent.Name);
                var toDelete = dbParent.SubComponents.FirstOrDefault(x => x.Parent.Name == children.Name);

                if (toDelete != null)
                {
                    dbParent.SubComponents.Remove(toDelete);
                    container.Store(dbParent);
                    container.Store(dbParent.SubComponents);
                    container.Delete(toDelete);
                    container.Commit();
                }
            }
        }

        public void RemoveComponent(ComponentList parent, ComponentList children)
        {
            using (var container = Db4oEmbedded.OpenFile(Resources.Database))
            {
                var dbParent = container.Query<ComponentList>().First(x => x.Name == parent.Name);
                var toDelete = dbParent.SubComponents.First(x => x.Parent.Name == children.Name);
                dbParent.SubComponents.Remove(toDelete);
                container.Store(dbParent);
                container.Store(dbParent.SubComponents);
                container.Delete(toDelete);
                container.Commit();
            }
        }

        #endregion
    }
}

