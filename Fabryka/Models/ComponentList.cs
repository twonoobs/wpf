﻿using System.Collections.Generic;

namespace Fabryka.Models
{
    public sealed class ComponentList
    {
        public string Name { get; set; }
        public string MeasureUnit { get; set; }
        public ICollection<Component> SubComponents { get; set; }
    }
}
