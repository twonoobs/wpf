﻿namespace Fabryka.Models
{
    public sealed class Component
    {
        public ComponentList Parent { get; set; }
        public double Quantity { get; set; }
    }
}
