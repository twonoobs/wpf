﻿namespace Fabryka.Models
{
    public class NeededModel
    {
        public string Name { get; set; }
        public string MeasureUnit { get; set; }
        public double Quantity { get; set; }
    }
}
