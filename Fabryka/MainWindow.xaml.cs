﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Windows;
using System.Windows.Controls;
using Db4objects.Db4o;
using Fabryka.Controllers;
using Fabryka.Models;

namespace Fabryka
{
    public partial class MainWindow 
    {
        //Controllers
        private DatabaseController DatabaseController { get;}
        private ValidationController ValidationController { get; }
        private DeleteController DeleteController { get; }

        //Lists
        public ObservableCollection<ComponentList> ComponentList { get; set; }
        public ObservableCollection<Component> SubComponentList { get; set; }

        private Dictionary<string, NeededModel> neededList = new Dictionary<string, NeededModel>();
    

        public MainWindow()
        {
            //Init ui
            InitializeComponent();

            //Init controllers
            DatabaseController = new DatabaseController();
            ValidationController = new ValidationController();
            DeleteController = new DeleteController();

            //Init list
            ComponentList = new ObservableCollection<ComponentList>();
            SubComponentList= new ObservableCollection<Component>();
            neededList = new Dictionary<string, NeededModel>();

            //Set binding
            ComponentListView.ItemsSource = ComponentList;

            //Load from database
            RefreshView();
        }

        #region UI

        public void RefreshView()
        {
            var selected = (ComponentList) ComponentListView.SelectedItem;

            ComponentList.Clear();

            foreach (var componentList in DatabaseController.GetComponentList())
            {
                ComponentList.Add(componentList);
            }

            if(selected != null)
            ComponentListView.SelectedItem = ComponentList.FirstOrDefault(x => x.Name == selected.Name);

            if (ComponentListView.SelectedItem != null && DatabaseController.GetComponentList().Any())
            {
                DrawTree((Models.ComponentList)ComponentListView.SelectedItem);
                neededList = new Dictionary<string, NeededModel>();
                ShowMaterialRequest((Models.ComponentList)ComponentListView.SelectedItem);
            }
            else
            {
                SubView.Items.Clear();
            }

            SubName.ItemsSource = DatabaseController.GetComponentList().Select(x => x.Name);
        }

        public void DrawTree(ComponentList element, bool header = true, TreeViewItem parent = null, double count = 0)
        {
            if (element == null) return;

            var tvi = new TreeViewItem
            {
                IsExpanded = true
            };

            if (header)
            {
                tvi.Header = $"{element.Name} - lista składowa".ToUpper();
            }
            else
            {
                tvi.Header = $"{element.Name} - ilość: {count} {element.MeasureUnit}".ToUpper();
            }

            parent?.Items.Add(tvi);

            if (element.SubComponents != null)
            {
                foreach (var item in element.SubComponents.OrderBy(x=>x.Parent.Name))
                {
                    DrawTree(item.Parent,false, tvi, item.Quantity);
                }
            }

            if (parent != null) return;
            SubView.Items.Clear();
            SubView.Items.Add(tvi);
        }

        private void ComponentListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComponentListView.SelectedItem != null)
            {
                DrawTree((ComponentList)ComponentListView.SelectedItem);
                neededList = new Dictionary<string, NeededModel>();
                ShowMaterialRequest((ComponentList)ComponentListView.SelectedItem);
            }
        }

        #endregion

        #region Product events

        private void AddProduct(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ComponentListName.Text) && !string.IsNullOrEmpty(ComponentListMeasureUnit.Text))
            {
                var tempComponent = new ComponentList() {Name = ComponentListName.Text.ToUpper(), MeasureUnit = ComponentListMeasureUnit.Text.ToUpper()};

                if (DatabaseController.CanAddComponentList(tempComponent))
                {
                    DatabaseController.AddComponentList(tempComponent);
                    ComponentListName.Text = string.Empty;
                    ComponentListMeasureUnit.Text = "SZT";
                    RefreshView();
                }
                else
                {
                    MessageBox.Show($"Lista składowa: {tempComponent.Name} już istnieje.", "Dodanie listy składowej", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                var information = "Wartości potrzebne do uzupełnienia:";
                if (string.IsNullOrEmpty(ComponentListName.Text)) information += "\n- Nazwa listy składowej.";
                if (string.IsNullOrEmpty(ComponentListMeasureUnit.Text)) information += "\n- Jednostka miary listy składowej.";
                MessageBox.Show(information, "Dodanie listy składowej", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void EditProduct(object sender, RoutedEventArgs e)
        {
            if (ComponentListView.SelectedItem != null)
            {
                var newComponentList = (ComponentList)ComponentListView.SelectedItem;
                var previousComponentList = newComponentList.Name;

                if (!string.IsNullOrEmpty(ComponentListName.Text)) newComponentList.Name = ComponentListName.Text.ToUpper();
                if (!string.IsNullOrEmpty(ComponentListMeasureUnit.Text)) newComponentList.MeasureUnit = ComponentListMeasureUnit.Text.ToUpper();

                DatabaseController.EditComponentList(newComponentList,previousComponentList);
                ComponentListName.Text = string.Empty;
                ComponentListMeasureUnit.Text = "SZT";
                RefreshView();
            }
            else
            {
                MessageBox.Show("Nie wybrano żadnej listy składowej.", "Edytowanie listy składowej", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void RemoveProduct(object sender, RoutedEventArgs e)
        {
            if (ComponentListView.SelectedItem != null)
            {
                DeleteController.DeleteComponent((Models.ComponentList)ComponentListView.SelectedItem);
                RefreshView();
            }
            else
            {
                MessageBox.Show("Nie wybrano żadnej listy składowej.","Usuwanie listy składowej",MessageBoxButton.OK,MessageBoxImage.Information);
            }
        }

        #endregion

        #region SubProduct events

        private void AddSubProduct(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SubName.Text) && !string.IsNullOrEmpty(SubQuantity.Text) && double.TryParse(SubQuantity.Text,out var quantity))
            {
                if (ComponentListView.SelectedItem != null)
                {
                    var parent = (ComponentList) ComponentListView.SelectedItem;

                    if (SubName.Text != parent.Name)
                    {
                        var children = ComponentList.First(x => x.Name == SubName.Text);

                        if (parent.SubComponents == null || parent.SubComponents.Count == 0)
                        {
                            if (!ValidationController.CheckUp(parent, children) && !ValidationController.CheckDown(parent, children))
                            {
                                DatabaseController.AddComponentWithList(parent, new Component { Parent = children, Quantity = quantity });
                                SubName.Text = string.Empty;
                                SubQuantity.Text = "1";
                                RefreshView();
                            }
                            else
                            {
                                MessageBox.Show("Wybrany skladnik jest składnikiem podrzędnym.", "Dodanie składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                            if (ValidationController.CheckList(parent.SubComponents, new Component { Parent = children, Quantity = quantity }))
                            {
                                if (!ValidationController.CheckUp(parent, children) && !ValidationController.CheckDown(parent, children))
                                {
                                    DatabaseController.AddComponentWithList(parent, new Component { Parent = children, Quantity = quantity });
                                    SubName.Text = string.Empty;
                                    SubQuantity.Text = "1";
                                    RefreshView();
                                }
                                else
                                {
                                    MessageBox.Show("Wybrany skladnik jest składnikiem podrzędnym.", "Dodanie składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Wybrany skladnik jest juz na liście.", "Dodanie składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Składnik nie może nazywać się tak samo jak lista składowa.", "Dodanie składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Nie zaznaczono żadnej listy składowej.", "Dodanie składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                var information = "Wartości potrzebne do uzupełnienia:";
                if (string.IsNullOrEmpty(SubName.Text)) information += "\n- Nazwa składnika.";
                if (string.IsNullOrEmpty(SubQuantity.Text) || !double.TryParse(SubQuantity.Text, out var temp)) information += "\n- Ilość składnika.";

                MessageBox.Show(information, "Dodanie składnika", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void EditSubProduct(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SubQuantity.Text) && double.TryParse(SubQuantity.Text, out var quantity))
            {
                if (ComponentListView.SelectedItem != null)
                {
                    if (SubView.SelectedItem != null)
                    {
                        var children = ComponentList.First(x => x.Name == ((TreeViewItem)SubView.SelectedItem).Header.ToString().Split('-')[0].Trim());
                        var parent = ComponentList.First(x => x.Name == ((TreeViewItem)((TreeViewItem) SubView.SelectedItem).Parent).Header.ToString().Split('-')[0].Trim());

                        DatabaseController.EditQComponentQuantity(parent,children,quantity);
                        RefreshView();
                    }
                    else
                    {
                        MessageBox.Show("Nie zaznaczono żadnego składnika.", "Edycja ilości składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Nie zaznaczono żadnej listy składowej.", "Edycja ilości składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Uzupełnij ilość składnika.", "Edycja ilości składnika", MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }

        private void RemoveSubProduct(object sender, RoutedEventArgs e)
        {
            if (ComponentListView.SelectedItem != null)
            {
                if (SubView.SelectedItem != null)
                {
                    var children = ComponentList.First(x => x.Name == ((TreeViewItem)SubView.SelectedItem).Header.ToString().Split('-')[0].Trim());
                    var parent = ComponentList.First(x => x.Name == ((TreeViewItem)((TreeViewItem)SubView.SelectedItem).Parent).Header.ToString().Split('-')[0].Trim());

                    DeleteController.DeleteComponent(parent,children);
                    RefreshView();
                }
                else
                {
                    MessageBox.Show("Nie zaznaczono żadnego składnika.", "Edycja ilości składnika", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Nie zaznaczono żadnej listy składowej.", "Edycja ilości składnika", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion

        #region Needed stuff

        private void ShowMaterialRequest(ComponentList parent, double parentCount = 1)
        {
            if (parent.SubComponents != null)
            {
                foreach (var item in parent.SubComponents)
                {
                    if (item.Parent != null)
                    {
                        ShowMaterialRequest(item.Parent, Convert.ToDouble(item.Quantity) * parentCount);
                    }
                }
            }
            else
            {
                if (!neededList.ContainsKey(parent.Name))
                {
                    var vall = new NeededModel
                    {
                        Quantity = parentCount,
                        MeasureUnit = parent.MeasureUnit,
                        Name = parent.Name
                    };

                    neededList.Add(parent.Name, vall);
                }
                else
                {
                    neededList[parent.Name].Quantity += parentCount;
                }
            }

            NeededListView.ItemsSource = neededList.Values.OrderBy(x=>x.Name);
        }

        #endregion
    }
}
